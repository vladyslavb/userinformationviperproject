//
//  RegisterStoryAssembly.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "RegisterStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "RegisterStoryViewController.h"
#import "RegisterStoryInteractor.h"
#import "RegisterStoryPresenter.h"
#import "RegisterStoryRouter.h"


@implementation RegisterStoryAssembly


#pragma mark - Initialization methods -

- (RegisterStoryViewController*) viewRegisterStory 
{
    return [TyphoonDefinition withClass: [RegisterStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterRegisterStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterRegisterStory]];
                                                    
                          }];
}

- (RegisterStoryInteractor*) interactorRegisterStory 
{
    return [TyphoonDefinition withClass: [RegisterStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterRegisterStory]];
                                                    
                          }];
}

- (RegisterStoryPresenter*) presenterRegisterStory
{
    return [TyphoonDefinition withClass: [RegisterStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewRegisterStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorRegisterStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerRegisterStory]];
                                                    
                          }];
}

- (RegisterStoryRouter*) routerRegisterStory
{
    return [TyphoonDefinition withClass: [RegisterStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewRegisterStory]];
                                                    
                          }];
}

@end
