//
//  RegisterStoryViewController.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "RegisterStoryViewInput.h"

// Protocols
@protocol RegisterStoryViewOutput;

@interface RegisterStoryViewController : UIViewController <RegisterStoryViewInput>

@property (nonatomic, strong) id<RegisterStoryViewOutput> output;

@end
