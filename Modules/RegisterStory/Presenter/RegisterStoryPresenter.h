//
//  RegisterStoryPresenter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "RegisterStoryViewOutput.h"
#import "RegisterStoryInteractorOutput.h"
#import "RegisterStoryModuleInput.h"

// Protocols
@protocol RegisterStoryViewInput;
@protocol RegisterStoryInteractorInput;
@protocol RegisterStoryRouterInput;

@interface RegisterStoryPresenter : NSObject <RegisterStoryModuleInput, RegisterStoryViewOutput, RegisterStoryInteractorOutput>

@property (nonatomic, weak) id<RegisterStoryViewInput> view;
@property (nonatomic, strong) id<RegisterStoryInteractorInput> interactor;
@property (nonatomic, strong) id<RegisterStoryRouterInput> router;

@end
