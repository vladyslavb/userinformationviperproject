//
//  RegisterStoryPresenter.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "RegisterStoryPresenter.h"

// Classes
#import "RegisterStoryViewInput.h"
#import "RegisterStoryInteractorInput.h"
#import "RegisterStoryRouterInput.h"

@implementation RegisterStoryPresenter


#pragma mark - Methods RegisterStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods RegisterStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods RegisterStoryInteractorOutput -


@end
