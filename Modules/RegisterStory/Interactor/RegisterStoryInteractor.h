//
//  RegisterStoryInteractor.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "RegisterStoryInteractorInput.h"

// Protocols
@protocol RegisterStoryInteractorOutput;

@interface RegisterStoryInteractor : NSObject <RegisterStoryInteractorInput>

@property (nonatomic, weak) id<RegisterStoryInteractorOutput> output;

@end
