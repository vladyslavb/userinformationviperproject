//
//  SettingsStoryAssembly.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "SettingsStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "SettingsStoryViewController.h"
#import "SettingsStoryInteractor.h"
#import "SettingsStoryPresenter.h"
#import "SettingsStoryRouter.h"


@implementation SettingsStoryAssembly


#pragma mark - Initialization methods -

- (SettingsStoryViewController*) viewSettingsStory 
{
    return [TyphoonDefinition withClass: [SettingsStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterSettingsStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterSettingsStory]];
                                                    
                          }];
}

- (SettingsStoryInteractor*) interactorSettingsStory 
{
    return [TyphoonDefinition withClass: [SettingsStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterSettingsStory]];
                                                    
                          }];
}

- (SettingsStoryPresenter*) presenterSettingsStory
{
    return [TyphoonDefinition withClass: [SettingsStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewSettingsStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorSettingsStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerSettingsStory]];
                                                    
                          }];
}

- (SettingsStoryRouter*) routerSettingsStory
{
    return [TyphoonDefinition withClass: [SettingsStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewSettingsStory]];
                                                    
                          }];
}

@end
