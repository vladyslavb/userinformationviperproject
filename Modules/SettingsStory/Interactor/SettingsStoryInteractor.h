//
//  SettingsStoryInteractor.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "SettingsStoryInteractorInput.h"

// Protocols
@protocol SettingsStoryInteractorOutput;

@interface SettingsStoryInteractor : NSObject <SettingsStoryInteractorInput>

@property (nonatomic, weak) id<SettingsStoryInteractorOutput> output;

@end
