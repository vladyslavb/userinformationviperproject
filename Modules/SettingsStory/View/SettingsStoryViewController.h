//
//  SettingsStoryViewController.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "SettingsStoryViewInput.h"

// Protocols
@protocol SettingsStoryViewOutput;

@interface SettingsStoryViewController : UIViewController <SettingsStoryViewInput>

@property (nonatomic, strong) id<SettingsStoryViewOutput> output;

@end
