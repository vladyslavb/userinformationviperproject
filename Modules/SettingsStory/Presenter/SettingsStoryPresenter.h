//
//  SettingsStoryPresenter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "SettingsStoryViewOutput.h"
#import "SettingsStoryInteractorOutput.h"
#import "SettingsStoryModuleInput.h"

// Protocols
@protocol SettingsStoryViewInput;
@protocol SettingsStoryInteractorInput;
@protocol SettingsStoryRouterInput;

@interface SettingsStoryPresenter : NSObject <SettingsStoryModuleInput, SettingsStoryViewOutput, SettingsStoryInteractorOutput>

@property (nonatomic, weak) id<SettingsStoryViewInput> view;
@property (nonatomic, strong) id<SettingsStoryInteractorInput> interactor;
@property (nonatomic, strong) id<SettingsStoryRouterInput> router;

@end
