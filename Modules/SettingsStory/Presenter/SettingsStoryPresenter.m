//
//  SettingsStoryPresenter.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "SettingsStoryPresenter.h"

// Classes
#import "SettingsStoryViewInput.h"
#import "SettingsStoryInteractorInput.h"
#import "SettingsStoryRouterInput.h"

@implementation SettingsStoryPresenter


#pragma mark - Methods SettingsStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods SettingsStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods SettingsStoryInteractorOutput -


@end
