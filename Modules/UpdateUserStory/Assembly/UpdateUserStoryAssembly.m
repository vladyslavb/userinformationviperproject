//
//  UpdateUserStoryAssembly.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "UpdateUserStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "UpdateUserStoryViewController.h"
#import "UpdateUserStoryInteractor.h"
#import "UpdateUserStoryPresenter.h"
#import "UpdateUserStoryRouter.h"


@implementation UpdateUserStoryAssembly


#pragma mark - Initialization methods -

- (UpdateUserStoryViewController*) viewUpdateUserStory 
{
    return [TyphoonDefinition withClass: [UpdateUserStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterUpdateUserStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterUpdateUserStory]];
                                                    
                          }];
}

- (UpdateUserStoryInteractor*) interactorUpdateUserStory 
{
    return [TyphoonDefinition withClass: [UpdateUserStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterUpdateUserStory]];
                                                    
                          }];
}

- (UpdateUserStoryPresenter*) presenterUpdateUserStory
{
    return [TyphoonDefinition withClass: [UpdateUserStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewUpdateUserStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorUpdateUserStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerUpdateUserStory]];
                                                    
                          }];
}

- (UpdateUserStoryRouter*) routerUpdateUserStory
{
    return [TyphoonDefinition withClass: [UpdateUserStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewUpdateUserStory]];
                                                    
                          }];
}

@end
