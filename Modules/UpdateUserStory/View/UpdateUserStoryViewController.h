//
//  UpdateUserStoryViewController.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "UpdateUserStoryViewInput.h"

// Protocols
@protocol UpdateUserStoryViewOutput;

@interface UpdateUserStoryViewController : UIViewController <UpdateUserStoryViewInput>

@property (nonatomic, strong) id<UpdateUserStoryViewOutput> output;

@end
