//
//  UpdateUserStoryInteractor.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "UpdateUserStoryInteractorInput.h"

// Protocols
@protocol UpdateUserStoryInteractorOutput;

@interface UpdateUserStoryInteractor : NSObject <UpdateUserStoryInteractorInput>

@property (nonatomic, weak) id<UpdateUserStoryInteractorOutput> output;

@end
