//
//  UpdateUserStoryPresenter.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "UpdateUserStoryPresenter.h"

// Classes
#import "UpdateUserStoryViewInput.h"
#import "UpdateUserStoryInteractorInput.h"
#import "UpdateUserStoryRouterInput.h"

@implementation UpdateUserStoryPresenter


#pragma mark - Methods UpdateUserStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods UpdateUserStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods UpdateUserStoryInteractorOutput -


@end
