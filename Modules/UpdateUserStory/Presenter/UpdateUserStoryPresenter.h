//
//  UpdateUserStoryPresenter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "UpdateUserStoryViewOutput.h"
#import "UpdateUserStoryInteractorOutput.h"
#import "UpdateUserStoryModuleInput.h"

// Protocols
@protocol UpdateUserStoryViewInput;
@protocol UpdateUserStoryInteractorInput;
@protocol UpdateUserStoryRouterInput;

@interface UpdateUserStoryPresenter : NSObject <UpdateUserStoryModuleInput, UpdateUserStoryViewOutput, UpdateUserStoryInteractorOutput>

@property (nonatomic, weak) id<UpdateUserStoryViewInput> view;
@property (nonatomic, strong) id<UpdateUserStoryInteractorInput> interactor;
@property (nonatomic, strong) id<UpdateUserStoryRouterInput> router;

@end
