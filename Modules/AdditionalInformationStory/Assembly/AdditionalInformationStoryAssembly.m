//
//  AdditionalInformationStoryAssembly.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "AdditionalInformationStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "AdditionalInformationStoryViewController.h"
#import "AdditionalInformationStoryInteractor.h"
#import "AdditionalInformationStoryPresenter.h"
#import "AdditionalInformationStoryRouter.h"


@implementation AdditionalInformationStoryAssembly


#pragma mark - Initialization methods -

- (AdditionalInformationStoryViewController*) viewAdditionalInformationStory 
{
    return [TyphoonDefinition withClass: [AdditionalInformationStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterAdditionalInformationStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterAdditionalInformationStory]];
                                                    
                          }];
}

- (AdditionalInformationStoryInteractor*) interactorAdditionalInformationStory 
{
    return [TyphoonDefinition withClass: [AdditionalInformationStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterAdditionalInformationStory]];
                                                    
                          }];
}

- (AdditionalInformationStoryPresenter*) presenterAdditionalInformationStory
{
    return [TyphoonDefinition withClass: [AdditionalInformationStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewAdditionalInformationStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorAdditionalInformationStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerAdditionalInformationStory]];
                                                    
                          }];
}

- (AdditionalInformationStoryRouter*) routerAdditionalInformationStory
{
    return [TyphoonDefinition withClass: [AdditionalInformationStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewAdditionalInformationStory]];
                                                    
                          }];
}

@end
