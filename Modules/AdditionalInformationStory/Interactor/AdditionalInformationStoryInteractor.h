//
//  AdditionalInformationStoryInteractor.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AdditionalInformationStoryInteractorInput.h"

// Protocols
@protocol AdditionalInformationStoryInteractorOutput;

@interface AdditionalInformationStoryInteractor : NSObject <AdditionalInformationStoryInteractorInput>

@property (nonatomic, weak) id<AdditionalInformationStoryInteractorOutput> output;

@end
