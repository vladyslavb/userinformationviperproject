//
//  AdditionalInformationStoryPresenter.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "AdditionalInformationStoryPresenter.h"

// Classes
#import "AdditionalInformationStoryViewInput.h"
#import "AdditionalInformationStoryInteractorInput.h"
#import "AdditionalInformationStoryRouterInput.h"

@implementation AdditionalInformationStoryPresenter


#pragma mark - Methods AdditionalInformationStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods AdditionalInformationStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods AdditionalInformationStoryInteractorOutput -


@end
