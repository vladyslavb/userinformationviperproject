//
//  AdditionalInformationStoryPresenter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AdditionalInformationStoryViewOutput.h"
#import "AdditionalInformationStoryInteractorOutput.h"
#import "AdditionalInformationStoryModuleInput.h"

// Protocols
@protocol AdditionalInformationStoryViewInput;
@protocol AdditionalInformationStoryInteractorInput;
@protocol AdditionalInformationStoryRouterInput;

@interface AdditionalInformationStoryPresenter : NSObject <AdditionalInformationStoryModuleInput, AdditionalInformationStoryViewOutput, AdditionalInformationStoryInteractorOutput>

@property (nonatomic, weak) id<AdditionalInformationStoryViewInput> view;
@property (nonatomic, strong) id<AdditionalInformationStoryInteractorInput> interactor;
@property (nonatomic, strong) id<AdditionalInformationStoryRouterInput> router;

@end
