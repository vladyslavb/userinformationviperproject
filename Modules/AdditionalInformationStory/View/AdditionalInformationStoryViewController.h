//
//  AdditionalInformationStoryViewController.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "AdditionalInformationStoryViewInput.h"

// Protocols
@protocol AdditionalInformationStoryViewOutput;

@interface AdditionalInformationStoryViewController : UIViewController <AdditionalInformationStoryViewInput>

@property (nonatomic, strong) id<AdditionalInformationStoryViewOutput> output;

@end
