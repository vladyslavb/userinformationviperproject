//
//  AdditionalInformationStoryViewOutput.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import Foundation;

// Protocols
@protocol AdditionalInformationStoryViewOutput <NSObject>

/**
 @author Vladyslav Bedro
 
 Method which inform presenter, that view is ready for a work
 */
- (void) didTriggerViewReadyEvent;

@end
