//
//  AvatarsGalleryStoryRouter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AvatarsGalleryStoryRouterInput.h"

// Protocols
@protocol RamblerViperModuleTransitionHandlerProtocol;

@interface AvatarsGalleryStoryRouter : NSObject <AvatarsGalleryStoryRouterInput>

@property (nonatomic, weak) id<RamblerViperModuleTransitionHandlerProtocol> transitionHandler;

@end
