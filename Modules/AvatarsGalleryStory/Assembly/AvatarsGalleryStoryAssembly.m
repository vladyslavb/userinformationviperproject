//
//  AvatarsGalleryStoryAssembly.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "AvatarsGalleryStoryAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "AvatarsGalleryStoryViewController.h"
#import "AvatarsGalleryStoryInteractor.h"
#import "AvatarsGalleryStoryPresenter.h"
#import "AvatarsGalleryStoryRouter.h"


@implementation AvatarsGalleryStoryAssembly


#pragma mark - Initialization methods -

- (AvatarsGalleryStoryViewController*) viewAvatarsGalleryStory 
{
    return [TyphoonDefinition withClass: [AvatarsGalleryStoryViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterAvatarsGalleryStory]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterAvatarsGalleryStory]];
                                                    
                          }];
}

- (AvatarsGalleryStoryInteractor*) interactorAvatarsGalleryStory 
{
    return [TyphoonDefinition withClass: [AvatarsGalleryStoryInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterAvatarsGalleryStory]];
                                                    
                          }];
}

- (AvatarsGalleryStoryPresenter*) presenterAvatarsGalleryStory
{
    return [TyphoonDefinition withClass: [AvatarsGalleryStoryPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewAvatarsGalleryStory]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorAvatarsGalleryStory]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerAvatarsGalleryStory]];
                                                    
                          }];
}

- (AvatarsGalleryStoryRouter*) routerAvatarsGalleryStory
{
    return [TyphoonDefinition withClass: [AvatarsGalleryStoryRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewAvatarsGalleryStory]];
                                                    
                          }];
}

@end
