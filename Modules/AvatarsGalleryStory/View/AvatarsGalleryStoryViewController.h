//
//  AvatarsGalleryStoryViewController.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "AvatarsGalleryStoryViewInput.h"

// Protocols
@protocol AvatarsGalleryStoryViewOutput;

@interface AvatarsGalleryStoryViewController : UIViewController <AvatarsGalleryStoryViewInput>

@property (nonatomic, strong) id<AvatarsGalleryStoryViewOutput> output;

@end
