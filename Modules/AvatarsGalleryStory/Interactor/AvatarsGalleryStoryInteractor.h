//
//  AvatarsGalleryStoryInteractor.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AvatarsGalleryStoryInteractorInput.h"

// Protocols
@protocol AvatarsGalleryStoryInteractorOutput;

@interface AvatarsGalleryStoryInteractor : NSObject <AvatarsGalleryStoryInteractorInput>

@property (nonatomic, weak) id<AvatarsGalleryStoryInteractorOutput> output;

@end
