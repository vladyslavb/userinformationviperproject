//
//  AvatarsGalleryStoryPresenter.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "AvatarsGalleryStoryPresenter.h"

// Classes
#import "AvatarsGalleryStoryViewInput.h"
#import "AvatarsGalleryStoryInteractorInput.h"
#import "AvatarsGalleryStoryRouterInput.h"

@implementation AvatarsGalleryStoryPresenter


#pragma mark - Methods AvatarsGalleryStoryModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods AvatarsGalleryStoryViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods AvatarsGalleryStoryInteractorOutput -


@end
