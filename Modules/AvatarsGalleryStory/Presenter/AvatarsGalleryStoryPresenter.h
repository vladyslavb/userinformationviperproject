//
//  AvatarsGalleryStoryPresenter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "AvatarsGalleryStoryViewOutput.h"
#import "AvatarsGalleryStoryInteractorOutput.h"
#import "AvatarsGalleryStoryModuleInput.h"

// Protocols
@protocol AvatarsGalleryStoryViewInput;
@protocol AvatarsGalleryStoryInteractorInput;
@protocol AvatarsGalleryStoryRouterInput;

@interface AvatarsGalleryStoryPresenter : NSObject <AvatarsGalleryStoryModuleInput, AvatarsGalleryStoryViewOutput, AvatarsGalleryStoryInteractorOutput>

@property (nonatomic, weak) id<AvatarsGalleryStoryViewInput> view;
@property (nonatomic, strong) id<AvatarsGalleryStoryInteractorInput> interactor;
@property (nonatomic, strong) id<AvatarsGalleryStoryRouterInput> router;

@end
