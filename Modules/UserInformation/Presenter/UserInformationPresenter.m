//
//  UserInformationPresenter.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "UserInformationPresenter.h"

// Classes
#import "UserInformationViewInput.h"
#import "UserInformationInteractorInput.h"
#import "UserInformationRouterInput.h"

@implementation UserInformationPresenter


#pragma mark - Methods UserInformationModuleInput -

- (void) configureModule 
{
	/**
	@author Vladyslav Bedro
	
	Starting configuration of the module, which dependent to the view state
	*/
}


#pragma mark - Methods UserInformationViewOutput -

- (void) didTriggerViewReadyEvent 
{
	[self.view setupInitialState];
}


#pragma mark - Methods UserInformationInteractorOutput -


@end
