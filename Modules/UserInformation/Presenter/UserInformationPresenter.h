//
//  UserInformationPresenter.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "UserInformationViewOutput.h"
#import "UserInformationInteractorOutput.h"
#import "UserInformationModuleInput.h"

// Protocols
@protocol UserInformationViewInput;
@protocol UserInformationInteractorInput;
@protocol UserInformationRouterInput;

@interface UserInformationPresenter : NSObject <UserInformationModuleInput, UserInformationViewOutput, UserInformationInteractorOutput>

@property (nonatomic, weak) id<UserInformationViewInput> view;
@property (nonatomic, strong) id<UserInformationInteractorInput> interactor;
@property (nonatomic, strong) id<UserInformationRouterInput> router;

@end
