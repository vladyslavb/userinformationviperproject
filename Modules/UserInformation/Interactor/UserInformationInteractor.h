//
//  UserInformationInteractor.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Classes
#import "UserInformationInteractorInput.h"

// Protocols
@protocol UserInformationInteractorOutput;

@interface UserInformationInteractor : NSObject <UserInformationInteractorInput>

@property (nonatomic, weak) id<UserInformationInteractorOutput> output;

@end
