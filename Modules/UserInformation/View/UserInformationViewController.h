//
//  UserInformationViewController.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
@import UIKit;

// Classes
#import "UserInformationViewInput.h"

// Protocols
@protocol UserInformationViewOutput;

@interface UserInformationViewController : UIViewController <UserInformationViewInput>

@property (nonatomic, strong) id<UserInformationViewOutput> output;

@end
