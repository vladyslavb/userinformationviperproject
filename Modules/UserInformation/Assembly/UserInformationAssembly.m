//
//  UserInformationAssembly.m
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

#import "UserInformationAssembly.h"

// Frameworks
#import <ViperMcFlurry/ViperMcFlurry.h>

// Classes
#import "UserInformationViewController.h"
#import "UserInformationInteractor.h"
#import "UserInformationPresenter.h"
#import "UserInformationRouter.h"


@implementation UserInformationAssembly


#pragma mark - Initialization methods -

- (UserInformationViewController*) viewUserInformation 
{
    return [TyphoonDefinition withClass: [UserInformationViewController class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterUserInformation]];
                              [definition injectProperty: @selector(moduleInput)
                                                    with: [self presenterUserInformation]];
                                                    
                          }];
}

- (UserInformationInteractor*) interactorUserInformation 
{
    return [TyphoonDefinition withClass: [UserInformationInteractor class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(output)
                                                    with: [self presenterUserInformation]];
                                                    
                          }];
}

- (UserInformationPresenter*) presenterUserInformation
{
    return [TyphoonDefinition withClass: [UserInformationPresenter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(view)
                                                    with: [self viewUserInformation]];
                              [definition injectProperty: @selector(interactor)
                                                    with: [self interactorUserInformation]];
                              [definition injectProperty: @selector(router)
                                                    with: [self routerUserInformation]];
                                                    
                          }];
}

- (UserInformationRouter*) routerUserInformation
{
    return [TyphoonDefinition withClass: [UserInformationRouter class]
                          configuration: ^(TyphoonDefinition* definition) {
                          
                              [definition injectProperty: @selector(transitionHandler)
                                                    with: [self viewUserInformation]];
                                                    
                          }];
}

@end
