//
//  UserInformationAssembly.h
//  UserInformationVIPERProject
//
//  Created by Vladyslav Bedro on 15/11/2018.
//  Copyright © 2018 OnSight. All rights reserved.
//

// Frameworks
#import <Typhoon/Typhoon.h>
#import <RamblerTyphoonUtils/AssemblyCollector.h>

/**
 @author Vladyslav Bedro

 UserInformation module
 */
@interface UserInformationAssembly : TyphoonAssembly <RamblerInitialAssembly>

@end
